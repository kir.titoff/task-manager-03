package ru.t1.ktitov.tm.constant;

public class TerminalConst {

    public final static String VERSION = "version";
    public final static String HELP = "help";
    public final static String ABOUT = "about";

}
